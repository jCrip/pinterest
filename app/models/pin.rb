class Pin < ActiveRecord::Base
  validates :photo, presence: true
  validates :title, presence: true
  validates :description, presence: true

  belongs_to :user

  after_create :update_slug
  before_create :assign_slug

  def to_param
    slug
  end

  private

  def assign_slug
    self.slug = "#{title.parameterize}"
  end

  def update_slug
    update_attributes slug: assign_slug
  end

end
