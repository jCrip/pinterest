json.array!(@pins) do |pin|
  json.extract! pin, :id, :photo, :title, :description
  json.url pin_url(pin, format: :json)
end
